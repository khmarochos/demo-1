// vim:set softtabstop=2 shiftwidth=2 tabstop=2 expandtab:

#include <stdio.h>
#include <unistd.h>



enum mode{Normal, Test};


/*
 *  Determines the running mode by command-line arguments
 */
enum mode get_mode(int argc, char **argv) {

  int option;

  while ((option = getopt(argc, argv, "t")) != -1) {
    switch(option) {
      case 't':
        return(Test);
    }
  }

  return(Normal);

}

/*
 *  Simulates self-testing
 */
unsigned int test() {

  unsigned int result = 0;

  setbuf(stdout, NULL);

  printf("Performing tests...\n");

  for (int i = 0; i < 3; i++) {
    printf(">>> Test #%d ", i);
    result = sleep(1);
    if(result == 0) {
      printf("PASSED!\n");
    } else {
      printf("FAILED!\n");
      return(result);
    }
  }

  printf("Everything is fine.\n");

  return(0);
}


/*
 * Says hello to the world
 */
unsigned int hello() {

  printf("Hello, world!\n");

  return(0);

}



/*
 * Determines the running mode and does what needs to be done
 */
int main(int argc, char **argv) {

  enum mode running_mode = get_mode(argc, argv);

  switch (running_mode) {
    case Test:
      return(test());
    case Normal:
      return(hello());
  }


}
