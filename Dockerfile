FROM scratch

ARG ARTIFACTS_DIRECTORY

COPY "${ARTIFACTS_DIRECTORY}/*" /

CMD ["/hello"]
