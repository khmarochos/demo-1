artifacts_directory = ${ARTIFACTS_DIRECTORY}
artifacts_to_save = hello
objects = hello.o
sources = hello.c

default: $(artifacts_to_save)

hello.o: $(sources)
	for source_file in $(sources); do \
		object_file="$$(echo "$${source_file}" | sed 's/\.c$$/.o/')"; \
		gcc -c "$${source_file}" -o "$${object_file}" ; \
	done

hello: $(objects)
	gcc $(objects) -static -o hello

save: $(artifacts_to_save)
	-mkdir artifacts
	cp $(artifacts_to_save) $(artifacts_directory)/

test: $(artifacts_to_save)
	for artifact_to_test in $(artifacts_to_save); do \
		"./$${artifact_to_test}" -t ; \
	done

clean:
	-rm -rf $(objects) $(artifacts_to_save) $(artifacts_directory)
